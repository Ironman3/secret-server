<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\DB;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    private const SECRET_KEY = 'my secret key';

    public function loadSecret(
        Request $request,
        $hash
    ) {
        $result = static::readSecret(
            $hash
        );
        return json_encode([
            $result['status'] ? $result['body'] : $result['response']
        ]);
    }

    public function storeSecret(
        Request $request
    ) {
        $result = static::createSecret(
            $request->input('secret'),
            $request->input('expire_after_views'),
            $request->input('expire_after')
        );
        return json_encode([
            $result['status'] ? $result['body'] : $result['response']
        ]);
    }

    private static function readSecret(
        $hash
    ) {
        $secretRecord = (DB::table('secrets')
            ->where('hash', '=', $hash)
            ->first()
        );
        if (!$secretRecord) {
            return [
                'status'    => false,
                'response'  => 'Invalid hash!',
                'body'      => null
            ];
        } else if($secretRecord->expire_after_views <= 0) {
            return [
                'status'    => false,
                'response'  => 'Too many access!',
                'body'      => null
            ];
        }
        else if(
            $secretRecord->expire_after > 0 &&
            ($expireDate = (new Carbon($secretRecord->created_at))->addMinutes($secretRecord->expire_after)->format('Y-m-d H:i:s')) < ($now = Carbon::now()->format('Y-m-d H:i:s'))
        ) {
            return [
                'status'    => false,
                'response'  => 'It is expired!',
                'body'      => null
            ];
        } else {
            --$secretRecord->expire_after_views;
            //---
            (DB::table('secrets')
                ->where('id', '=', $secretRecord->id)
                ->update([
                    'expire_after_views'    => $secretRecord->expire_after_views,
                    'updated_at'            => $now
                ])
            );
            return [
                'status'    => true,
                'response'  => 'Success operation!',
                'body'      => [
                    'hash'              => $secretRecord->hash,
                    'secretText'        => static::decode($secretRecord->secret_text),
                    'createdAt'         => $secretRecord->created_at,
                    'expiresAt'         => $expireDate,
                    'remainingViews'    => $secretRecord->expire_after_views
                ]
            ];
        }
    }

    private static function createSecret(
        $secretText,
        $expireAfterViews,
        $expireAfter
    ) {
        $expireAfterViews   = (int)$expireAfterViews;
        $expireAfter        = (int)$expireAfter;
        //---
        if(empty($secretText)) {
            return [
                'status'    => false,
                'response'  => 'Empty secret field!',
                'body'      => null
            ];
        } else if($expireAfterViews <= 0) {
            return [
                'status'    => false,
                'response'  => 'Access number must be positive!',
                'body'      => null
            ];
        } else if($expireAfter < 0) {
            return [
                'status'    => false,
                'response'  => 'Access time must be positive!',
                'body'      => null
            ];
        }
        //---
        $hash   = uniqid('', true);
        $now    = Carbon::now();
        //---
        (DB::table('secrets')
            ->insert([
                'hash'                  => $hash,
                'secret_text'           => static::encode($secretText),
                'expire_after_views'    => $expireAfterViews,
                'expire_after'          => $expireAfter,
                'created_at'            => $now,
                'updated_at'            => $now
            ])
        );
        return [
            'status'    => true,
            'response'  => 'Success operation!',
            'body'      => [
                'hash'              => $hash,
                'secretText'        => $secretText,
                'createdAt'         => $now->format('Y-m-d H:i:s'),
                'expiresAt'         => $now->addMinutes($expireAfter)->format('Y-m-d H:i:s'),
                'remainingViews'    => $expireAfterViews
            ]
        ];
    }

    private static function decode(
        $cipherText
    ) {
        $key            = static::SECRET_KEY;
        $cipher         = "AES-128-CBC";
        $tmp            = base64_decode($cipherText);
        $ivLen          = 16;
        $iv             = substr($tmp, 0, $ivLen);
        $cipherTextRaw  = substr($tmp, $ivLen);
        //---
        $originalText = openssl_decrypt(
            $cipherTextRaw, 
            $cipher, 
            $key, 
            OPENSSL_RAW_DATA, 
            $iv
        );
        //---
        return $originalText;
    }

    private static function encode(
        $text
    ) {

        $key    = static::SECRET_KEY;
        $cipher = "AES-128-CBC";
        $ivLen  = 16;
        $iv     = openssl_random_pseudo_bytes($ivLen);
        //---
        $cipherTextRaw = openssl_encrypt(
            $text, 
            $cipher, 
            $key, 
            OPENSSL_RAW_DATA, 
            $iv
        );
        //---
        return base64_encode($iv . $cipherTextRaw);
    }
}
